package com.pjatk.s9314.list;

import android.provider.ContactsContract;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    private static final int EDIT = 0, DELETE = 1;

    EditText nameInput, phoneInput, debtInput;
    List<ListClass> Peoples = new ArrayList<ListClass>();
    ListView peopleListView;
    DbHandler dbHandler;
    int longClickedItemIndex;
    ArrayAdapter<ListClass> listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nameInput = (EditText) findViewById(R.id.nameInput);
        phoneInput = (EditText) findViewById(R.id.phoneInput);
        debtInput = (EditText) findViewById(R.id.debtInput);
        peopleListView = (ListView) findViewById(R.id.listView);
        TabHost tabHost = (TabHost) findViewById(R.id.tabHost);
        dbHandler = new DbHandler(getApplicationContext());

        registerForContextMenu(peopleListView);


        peopleListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                longClickedItemIndex = position;
                return false;
            }
        });


        tabHost.setup();

        TabHost.TabSpec tabSpec = tabHost.newTabSpec("createTab");
        tabSpec.setContent(R.id.createTab);
        tabSpec.setIndicator("Dodaj osobe");
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec("userListTab");
        tabSpec.setContent(R.id.userListTab);
        tabSpec.setIndicator("Pokaz liste");
        tabHost.addTab(tabSpec);

        final Button addBtn = (Button) findViewById(R.id.addBtn);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ListClass listClass = new ListClass(dbHandler.getPeopleCount(), String.valueOf(nameInput.getText()), String.valueOf(phoneInput.getText()), String.valueOf(debtInput.getText()));
                if(!peopleExist(listClass)){
                    dbHandler.createPeople(listClass);
                    Peoples.add(listClass);
                    listAdapter.notifyDataSetChanged();
        //                addPeople(nameInput.getText().toString(), phoneInput.getText().toString(), debtInput.getText().toString());
                    Toast.makeText(getApplicationContext(), "Dodałeś " + String.valueOf(nameInput.getText()) + ". Dług wynosi " + String.valueOf(debtInput.getText()), Toast.LENGTH_SHORT).show();
                    return;
                }
                Toast.makeText(getApplicationContext(), String.valueOf(nameInput.getText()) + " już istnieje.", Toast.LENGTH_SHORT).show();
            }
        });

        nameInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                addBtn.setEnabled(String.valueOf(nameInput.getText()).trim().length() > 0);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
//        List<ListClass> addableContacts = dbHandler.getAllPeople();
//        int contactCount = dbHandler.getPeopleCount();
//
//        for (int i = 0; i < contactCount; i++){
//            Peoples.add(addableContacts.get(i));
//        }
        if(dbHandler.getPeopleCount() != 0)
            Peoples.addAll(dbHandler.getAllPeople());

        populateList();

    }

    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, view, menuInfo);

        menu.setHeaderIcon(R.drawable.edit);
        menu.setHeaderTitle("Edytuj");
        menu.add(Menu.NONE, EDIT, menu.NONE, "Edytuj osobę");
        menu.add(Menu.NONE, DELETE, menu.NONE, "Usuń osobę");
    }

    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case EDIT:
                //TODO: Implement Editing contact
                break;
            case DELETE:
                dbHandler.deletePeople(Peoples.get(longClickedItemIndex));
                Peoples.remove(longClickedItemIndex);
                listAdapter.notifyDataSetChanged();
                break;
        }
        return super.onContextItemSelected(item);
    }

    private boolean peopleExist(ListClass listClass) {
        String name = listClass.get_name();
        int peopleCount = Peoples.size();
        for(int i = 0; i < peopleCount; i++) {
            if(name.compareToIgnoreCase(Peoples.get(i).get_name()) == 0);
        }
        return false;
    }

    private void populateList() {
        listAdapter = new ListClassAdapter();
        peopleListView.setAdapter(listAdapter);
    }

//    private void addPeople(String name, String phone, String debt) {
//        Peoples.add(new ListClass(name,phone,debt));
//    }

    private class ListClassAdapter extends ArrayAdapter<ListClass> {
        public ListClassAdapter() {
            super(MainActivity.this, R.layout.listview_item, Peoples);
        }

        @Override
        public View getView(int position, View view, ViewGroup parent){
            if (view == null) {
                view = getLayoutInflater().inflate(R.layout.listview_item, parent, false);
            }
            ListClass currentPeople = Peoples.get(position);

            TextView name = (TextView) view.findViewById(R.id.cName);
            name.setText(currentPeople.get_name());
            TextView phone = (TextView) view.findViewById(R.id.cPhone);
            phone.setText(currentPeople.get_phone());
            TextView debt = (TextView) view.findViewById(R.id.cDebt);
            debt.setText(currentPeople.get_debt());

            return view;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
