package com.pjatk.s9314.list;

/**
 * Created by Maksymilian on 2015-06-19.
 */
public class ListClass {

    private String _name, _phone, _debt;
    private int _id;

    public ListClass (int id, String name, String phone, String debt) {
        _id = id;
        _name = name;
        _phone = phone;
        _debt = debt;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_phone() {
        return _phone;
    }

    public void set_phone(String _phone) {
        this._phone = _phone;
    }

    public String get_debt() {
        return _debt;
    }

    public void set_debt(String _debt) {
        this._debt = _debt;
    }

    public int getId() {
        return _id;
    }

    public void setId(int _id) {
        this._id = _id;
    }
}
