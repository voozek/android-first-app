package com.pjatk.s9314.list;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Maksymilian on 2015-06-19.
 */
public class DbHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "listdb",
    TABLE_LIST = "listdb",
    KEY_ID = "id",
    KEY_NAME = "name",
    KEY_PHONE = "phone",
    KEY_DEBT = "debt";

    public DbHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_LIST + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_NAME + " TEXT," + KEY_PHONE + " TEXT," + KEY_DEBT + " TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_LIST);
        onCreate(db);
    }

    public void createPeople (ListClass listClass) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_NAME, listClass.get_name());
        values.put(KEY_PHONE, listClass.get_phone());
        values.put(KEY_DEBT, listClass.get_debt());

        db.insert(TABLE_LIST, null, values);
        db.close();
    }

    public ListClass getPeople(int id) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(TABLE_LIST, new String[] { KEY_ID, KEY_NAME, KEY_PHONE, KEY_DEBT}, KEY_ID + "=?", new String[] { String.valueOf(id)} , null, null, null, null );

        if(cursor != null)
            cursor.moveToFirst();

        ListClass listClass = new ListClass(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3));
        db.close();
        cursor.close();
        return listClass;
    }

    public void deletePeople(ListClass listClass) {
        SQLiteDatabase db = getWritableDatabase();

        db.delete(TABLE_LIST, KEY_ID + "=?", new String[] {String.valueOf(listClass.getId())});

        db.close();
    }

    public int getPeopleCount() {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_LIST, null);
        int count = cursor.getCount();
        db.close();
        cursor.close();

        return count;
    }

    public int updatePeople(ListClass listClass) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_NAME, listClass.get_name());
        values.put(KEY_PHONE, listClass.get_phone());
        values.put(KEY_DEBT, listClass.get_debt());

        int rowsAffected = db.update(TABLE_LIST, values, KEY_ID + "=?", new String[]{String.valueOf(listClass.getId())});

        db.close();

        return rowsAffected;
    }

    public List<ListClass> getAllPeople() {
        List<ListClass> peoples = new ArrayList<ListClass>();

        SQLiteDatabase db = getWritableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_LIST, null);

        if(cursor.moveToFirst()) {
            do {
                peoples.add(new ListClass(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3)));
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return peoples;
    }
}
